// import coursesData from '../data/coursesData';
import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	const [ courses, setCourses ] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the ccourses state to map the data retrieved from the fetch request into several course card
			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp = {course}/>
				)
			}))
		})
	}, [])

	// The map method loops through the individual course objects in our array and returns a course card component for each course.
	// Multiple components created through the map method must have a unique key that will help react js identify which component or elements have been changed, added, or removed.
	// Everytime the map method loops through the data, it creates a course card component and then passes the current element in our courses using the course prop.
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp = {course}/>
	// 	)
	// })

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}