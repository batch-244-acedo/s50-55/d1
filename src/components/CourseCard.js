import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button, Col, Alert } from 'react-bootstrap';

export default function CourseCard({ courseProp }) {
  // Deconstruct the course properties into their own variables
  const { _id, name, description, price } = courseProp;

  // Syntax: const [getter, setter] = useState(initialGetterValue)
  // const [count, setCount] = useState(0);
  // const [ isOpen, setIsOpen] = useState(false);
  // const [seats, setSeats] = useState(30);

  // Function that keeps track of the enrollees for a course
  // function enroll(){
  //       // if (seats > 0) {
  //           setCount(count + 1);
  //           console.log('Enrollees: ' + count);
  //           setSeats(seats - 1);
  //           console.log('Seats: ' + seats);
        // } else {
        //  alert('No more seats available');
        // }
        
    // }
  // Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
    // This is run automatically both after initial render and for every DOM update
    // React will re-run this effect ONLY if any of the values contained in the seats array has changed from the last render / update    
  // useEffect (() => {
  //   if (seats === 0) {
  //       setIsOpen(true);
  //   }
  // }, [seats]);

  return (
    <Card>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>Php {price}</Card.Text>
          {/*<Card.Text>Seats: {seats}</Card.Text>*/}
          <Button as={Link} variant="primary" to={`/courses/${_id}`}>
            Details
          </Button>
        </Card.Body>
    </Card>
  )
}